# Git workshop

**[Useful Git Cheatsheet](http://rogerdudler.github.io/git-guide/)**

**[Official ProGit book](http://rogerdudler.github.io/git-guide/)**

## Scenario 1: create new repo for my project

> Everytime I want to version my project, I can create local git repository and start versioning it.
It could be Java project, but it could be my bachelor thesis written in latex as well :)

**Goal**: Create new maven project with your IDE and push it into new gitlab repository.

### Steps
* create new maven project in IntelliJ idea
* initialize new local git repo in this project
* create file `.gitignore` and ignore `target/` folder and IDE project files.
* init commit (all source files, `pom.xml`, `.gitignore`, ...)
* create repository on the internet, feel free to use faculty gitlab (https://gitlab.fi.muni.cz)
* initial push to your your repository
* done ;)


## Scenario 2: Work with multiple repositories

> Imagine you want to contribute to some opensource project.
You found a bug in a library you use and you want to share your fix with the community.
You fork this repository and then open Merge Request back to original (upstream) project.

> We use similar workflow in PB162. There is upstream repository where teacher provides iteration assignment.
Only difference is that you want to push your implementation to your private fork instead of upstream.


**Origin**: usually the name of your repository on gitlab/github
**Upstream**: usually the main community repository


You usually fork upstream repository and you call your copy origin.

**Goal**: Excercise working with multiple repositories like we do in PB162

### Steps
1. Create a fork of this repository
1. Clone *your* repository

```bash
$ git clone <git@gitlab.fi.muni.cz:<username>/git-workshop.git>
$ cd git-workshop
$ git status
```

### Steps for developer A
1. Create new branch ```dollar-converter``` based on ```master```
1. Implement the class ```DollarConverter```
1. Commit your work

    ```bash
    $ git status
    $ git add .
    $ git status
    $ git commit -m "Implementation of dollar converter"
    ```

1. Push ```dollar-converter``` branch to **your** Gitlab repository

    ```bash
    $ git push origin dollar-converter
    ```
1. Open merge request from ```dollar-converter``` branch in **your** at Gitlab to ```master``` branch of **upstream** repo at Gitlab


### Steps for developer B
1. Create new branch ```eur-converter``` based on ```master```
1. Implement the class ```EurConverter```
1. Commit your work

    ```bash
    $ git status
    $ git add .
    $ git status
    $ git commit -m "Implementation of dollar converter"
    ```

1. Push ```eur-converter``` branch to **your** Gitlab repository

    ```bash
    $ git push origin dollar-converter
    ```
1. Open merge request from ```eur-converter``` branch in **your** at Gitlab to ```master``` branch of **upstream** repo at Gitlab


### Combine the work of A and B
1. Wait for the owner of the **upstream** repository to merge both contributions
1. Checkout the master branch in **your local** repostiory
1. Pull the changes from **upstream** repo at GitLab (You will need to add it to your local repository as another remote)

```
git remote show
git remote add upstream git@gitlab.fi.muni.cz:pb162/git-workshop.git
git pull upstream master
```
1. Push the combined code into **your** master branch at Gitlab
