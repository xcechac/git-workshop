package cz.muni.fi.pb162.git;

/**
 * Class represents a converter to and from czech crowns
 *
 * @author Jakub Cechacek
 */
public interface Converter {

    /**
     * @return amount of czech crowns per unit of foreign currency
     */
    double getConvertRatio();

    /**
     * Convert currency amount to czech crowns
     *
     * @param amount amount in foreign currency
     * @return amount in czech crowns
     */
    double toCzk(double amount);

    /**
     * Convert czech crown to foreign currency
     *
     * @param amount amount czech crowns
     * @return amount in foreign currency
     */
    double fromCzk(double amount);

}
