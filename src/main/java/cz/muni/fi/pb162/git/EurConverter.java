package cz.muni.fi.pb162.git;

public class EurConverter implements Converter {

    private static final double RATIO = 26.75;

    @Override
    public double getConvertRatio() {
        return RATIO;
    }

    @Override
    public double toCzk(double amount) {
        return amount * RATIO;
    }

    @Override
    public double fromCzk(double amount) {
        return amount / RATIO;
    }
}
